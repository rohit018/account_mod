from rest_framework import serializers
from .models import Account
from otp.models import OTP
from rest_framework.exceptions import ValidationError
from random import randint
from django.core.mail import EmailMessage
from celery import shared_task

class UserCreateSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()
    mobile_no = serializers.CharField()
    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)
    
    class Meta:
        model = Account
        fields = ['email','mobile_no','first_name',"last_name","password1","password2"]
    
    def check_if_exists(self,email,mobile_no):
        email_status = Account.objects.filter(email=email).exists()
        mobile_status = Account.objects.filter(mobile_no=mobile_no).exists()
        if email_status == True or mobile_status == True:
            return True
        return False

    @shared_task
    def send_otp_mail(email,otp):
        try:
            mail_subject = "[OTP] Activate Your Account with this OTP"
            message = "Thank you for signing up, please verify with the OTP "+otp
            to_email = email
            email = EmailMessage(mail_subject,message,to=[to_email])
            email.send()
            return True
        except Exception as exc:
            print(exc)
            return False


    def GenerateOTP(self,email):
        otp = randint(111111,999999)
        OTP.objects.create(email=email,otp=otp)
        try:
            otp_status = self.send_otp_mail.delay(email,str(otp))
            return otp_status
        except Exception as esc:
            print(esc)
            pass

    def create(self, validated_data):
        email = validated_data['email']
        mobile_no = validated_data['mobile_no']
        first_name = validated_data['first_name']
        last_name = validated_data['last_name']
        if validated_data['password1'] == validated_data['password2']:
            password = validated_data['password1']
        else:
            raise ValidationError(
                {'Email': ["Both Passwords dont match"]})
        # Check if user already exist
        if self.check_if_exists(email,mobile_no) == True:
            raise ValidationError(
                {'Email': ["User with this details already exists"]}
                )
        # print("Email,mb is :- ",email,mobile_no)
        # Generate OTP for user
        self.GenerateOTP(email)
        return Account.objects.create_user(email=email,mobile_no=mobile_no,first_name=first_name,last_name=last_name,password=password)
