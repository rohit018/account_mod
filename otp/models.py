from django.db import models

# Create your models here.
class OTP(models.Model):
    email = models.EmailField(max_length=255,null=False)
    otp = models.CharField(max_length=6,null=False)
    OTPGenerated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.email